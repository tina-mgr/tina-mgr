# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: GPL-2.0-or-later
"""A set of unit tests for the tina-convert command-line tool."""

from __future__ import annotations

import pytest


pytest.register_assert_rewrite("unit.convert.util")
