#![deny(missing_docs)]
#![deny(clippy::missing_docs_in_private_items)]
// SPDX-FileCopyrightText: Peter Pentchev
// SPDX-License-Identifier: GPL-2.0-or-later
//! Utilities for handling Tina database files in different formats.

pub mod convert;
pub mod db;
pub mod defs;
