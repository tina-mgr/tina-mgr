<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: GPL-2.0-or-later
-->

# Python API: Common definitions

::: tina_mgr.defs.VERSION

::: tina_mgr.defs.Error
