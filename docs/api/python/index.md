<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: GPL-2.0-or-later
-->

# API Reference

- [Common definitions](common.md)
- [The in-memory representation of the Tina database](db.md)
- [Parsing the native Tina database](parse.md)
- [Conversion: format handlers](convert-formats.md)
- [Conversion: errors](convert-errors.md)
