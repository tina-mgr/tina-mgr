<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: GPL-2.0-or-later
-->

# Python API: Parsing the native Tina database

::: tina_mgr.parse.ParseError

::: tina_mgr.parse.loads
