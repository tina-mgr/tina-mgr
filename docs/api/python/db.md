<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: GPL-2.0-or-later
-->

# Python API: The in-memory Tina representation of the Tina database

::: tina_mgr.db.TinaEntry
