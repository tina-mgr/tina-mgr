<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: GPL-2.0-or-later
-->

# Python API: Conversion: errors

::: tina_mgr.convert.formats.f_base.FormatError

::: tina_mgr.convert.formats.f_base.DeserializationError

::: tina_mgr.convert.formats.f_base.NoFormatVersionError

::: tina_mgr.convert.formats.f_base.UnsupportedFormatVersionError

::: tina_mgr.convert.formats.f_base.NoTinaError

::: tina_mgr.convert.formats.f_base.NoTinaListError

::: tina_mgr.convert.formats.f_base.NoFieldError

::: tina_mgr.convert.formats.f_base.InvalidTypeFieldError

::: tina_mgr.convert.formats.f_json.JSONDeserializationError
