<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: GPL-2.0-or-later
-->

# Python API: Conversion: format handlers

::: tina_mgr.convert.formats.f_base.FormatHandler

::: tina_mgr.convert.formats.f_json.JSONFormatHandler

::: tina_mgr.convert.formats.f_tina.TinaFormatHandler

::: tina_mgr.convert.formats.f_yaml.YAMLFormatHandler
