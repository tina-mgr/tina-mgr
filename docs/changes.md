<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: GPL-2.0-or-later
-->

# Changelog

All notable changes to the tina project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.14] - 2024-12-27

### Fixes

- Bring back the `AUTHORS` and `NEWS` files; `automake` requires them unconditionally
- Correct the C `printf()` format specifiers for `size_t` variables
- Fix the generation of the `Item-ID` attribute when the `time_t` type is wider than
  the `unsigned long` one
- Do not create items with duplicate `Item-ID`
- Python utilities:
    - explicitly list the files to be included in the `sdist` tarball
    - check for items with duplicate `Item-ID` when loading a database
- Rust utilities:
    - handle items and categories listed out of order in the native database
    - check for items with duplicate `Item-ID` when loading a database

### Additions

- Seed the C library's random generator, use `/dev/urandom` or `/dev/random` if available
- Add more tests to the Python `tmux` test suite
- Python utilities:
    - show the exception type for unexpected errors

### Other changes

- Add the files generated by the `automake` / `autoconf` build system to the Git ignore file
- For the Python `tmux` tests, use `libtmux` 0.40.1; the environment fix was accepted upstream

## [0.1.13] - 2024-12-22

### Fixes

- Use `AC_SYS_LARGEFILE` in the configure script instead of relying on
  getconf(1): the latter may even be wrong when cross-building

### Additions

- Add a basic unit tests suite
- Add a `tina-convert` tool for exporting and importing the tina
  database into JSON and YAML
- Add some `MkDocs` documentation

## [0.1.12] - 2016-04-12

### Fixes

- Refresh the autotools packaging files
- Remove the arguments from the `AM_INIT_AUTOMAKE` invocation
- Fix some compiler warnings

### Additions

- Let the configure script also add `-Wstrict-prototypes` if possible
- Let the configure script check for Large File Support
- Add some copyright notices

### Other changes

- Adopted by Peter Pentchev

## [0.1.11] - 2007-08-16

### Other changes

- Last release within Debian by Matt Kraai

[Unreleased]: https://gitlab.com/tina-mgr/tina-mgr/-/compare/release%2F0.1.14...master
[0.1.14]: https://gitlab.com/tina-mgr/tina-mgr/-/compare/release%2F0.1.13...release%2F0.1.14
[0.1.13]: https://gitlab.com/tina-mgr/tina-mgr/-/compare/release%2F0.1.12...release%2F0.1.13
[0.1.12]: https://gitlab.com/tina-mgr/tina-mgr/-/compare/debian%2F0.1.11...release%2F0.1.12
[0.1.11]: https://gitlab.com/tina-mgr/tina-mgr/-/tags/debian%2F0.1.11
