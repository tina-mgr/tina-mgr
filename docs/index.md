<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: GPL-2.0-or-later
-->

# tina - the personal information manager

\[[Home][ringlet-home] | [Download](download.md) | [GitLab][gitlab] | [PyPI][pypi] | [crates.io][crates-io] | [Python API reference](api/python/index.md) | [ReadTheDocs][readthedocs]\]

## Overview

Tina is a personal information manager with a curses interface.
It allows the user to categorize short text items and to display
the items in a particular category.

## The tina-convert utility

The `tina-convert` utility may be used to convert the native Tina database to
and from other data representation formats, e.g. JSON or YAML, so that
its contents may be examined or modified using existing tools and libraries.

## Contact

Tina was initially developed by [Matt Kraai][kraai] within Debian, but it was
broken out as a separate software package at version 0.1.11 by
[Peter Pentchev][roam].
It is developed in [a GitLab repository][gitlab]. This documentation is
hosted at [Ringlet][ringlet-home] with a copy at [ReadTheDocs][readthedocs].

[roam]: mailto:roam@ringlet.net "Peter Pentchev"
[kraai]: mailto:kraai@debian.org "Matt Kraai"
[gitlab]: https://gitlab.com/tina-mgr/tina-mgr "The tina GitLab repository"
[pypi]: https://pypi.org/project/tina-mgr/ "The tina-mgr Python Package Index page"
[crates-io]: https://crates.io/crates/tina-mgr "The tina-mgr crate on crates.io"
[readthedocs]: https://tina-mgr.readthedocs.io/ "The tina ReadTheDocs page"
[ringlet-home]: https://devel.ringlet.net/misc/tina/ "The Ringlet tina homepage"
