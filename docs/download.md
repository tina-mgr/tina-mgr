<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: GPL-2.0-or-later
-->

# Download

These are the released versions of [tina](index.md) available for download.

## [0.1.14] - 2024-12-22

### Source tarball

- [tina-0.1.14.tar.gz](https://devel.ringlet.net/files/misc/tina/tina-0.1.14.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/tina/tina-0.1.14.tar.gz.asc))
- [tina-0.1.14.tar.bz2](https://devel.ringlet.net/files/misc/tina/tina-0.1.14.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/tina/tina-0.1.14.tar.bz2.asc))
- [tina-0.1.14.tar.xz](https://devel.ringlet.net/files/misc/tina/tina-0.1.14.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/tina/tina-0.1.14.tar.xz.asc))

## Python module

- [tina_mgr-0.1.14.tar.gz](https://devel.ringlet.net/files/misc/tina/tina_mgr-0.1.14.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/tina/tina_mgr-0.1.14.tar.gz.asc))
- [tina_mgr-0.1.14-py3-none-any.whl](https://devel.ringlet.net/files/misc/tina/tina_mgr-0.1.14-py3-none-any.whl)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/tina/tina_mgr-0.1.14-py3-none-any.whl.asc))

## [0.1.13] - 2024-12-22

### Source tarball

- [tina-0.1.13.tar.gz](https://devel.ringlet.net/files/misc/tina/tina-0.1.13.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/tina/tina-0.1.13.tar.gz.asc))
- [tina-0.1.13.tar.bz2](https://devel.ringlet.net/files/misc/tina/tina-0.1.13.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/tina/tina-0.1.13.tar.bz2.asc))
- [tina-0.1.13.tar.xz](https://devel.ringlet.net/files/misc/tina/tina-0.1.13.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/tina/tina-0.1.13.tar.xz.asc))

## Python module

- [tina_mgr-0.1.13.tar.gz](https://devel.ringlet.net/files/misc/tina/tina_mgr-0.1.13.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/tina/tina_mgr-0.1.13.tar.gz.asc))
- [tina_mgr-0.1.13-py3-none-any.whl](https://devel.ringlet.net/files/misc/tina/tina_mgr-0.1.13-py3-none-any.whl)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/tina/tina_mgr-0.1.13-py3-none-any.whl.asc))

## [0.1.12] - 2016-04-12

### Source tarball

- [tina-0.1.12.tar.gz](https://devel.ringlet.net/files/misc/tina/tina-0.1.12.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/tina/tina-0.1.12.tar.gz.asc))
- [tina-0.1.12.tar.bz2](https://devel.ringlet.net/files/misc/tina/tina-0.1.12.tar.bz2)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/tina/tina-0.1.12.tar.bz2.asc))
- [tina-0.1.12.tar.xz](https://devel.ringlet.net/files/misc/tina/tina-0.1.12.tar.xz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/tina/tina-0.1.12.tar.xz.asc))

## [0.1.11] - 2007-08-16

### Source tarball

- [tina-0.1.11.tar.gz](https://devel.ringlet.net/files/misc/tina/tina-0.1.11.tar.gz)
  (with [a PGP signature](https://devel.ringlet.net/files/misc/tina/tina-0.1.11.tar.gz.asc))

[0.1.14]: https://gitlab.com/tina-mgr/tina-mgr/-/tags/release%2F0.1.14
[0.1.13]: https://gitlab.com/tina-mgr/tina-mgr/-/tags/release%2F0.1.13
[0.1.12]: https://gitlab.com/tina-mgr/tina-mgr/-/tags/release%2F0.1.12
[0.1.11]: https://gitlab.com/tina-mgr/tina-mgr/-/tags/debian%2F0.1.11
