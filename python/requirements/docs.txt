# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: GPL-2.0-or-later

mkdocs >= 1.4.2, < 2
mkdocs-material >= 9.1.2, < 10
mkdocstrings >= 0.25, < 0.26
mkdocstrings-python >= 1, < 2
