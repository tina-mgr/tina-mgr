# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: GPL-2.0-or-later

click >= 8.1.3, < 9
pyparsing >= 3.0.9, < 4
pyyaml >= 6, < 7
